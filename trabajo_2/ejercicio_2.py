#___author___"Gabriela Viñan"
#___ email___ "gabriela.vinan@unl.edu.ec"
#Reescribe el programa del salario usando try y except, de modo que elprograma sea capaz de gestionar
# entradas no numéricas con elegancia, mostrando un mensaje y saliendo del programa.

try:
    horas = int(input("Ingrese el numero de horas: "))
    tarifa = float(input("Ingrese la tarifa: "))
    salario = float(horas * tarifa)
    if horas > 40:
        horasextra = horas - 40
        valorextra = (tarifa * 1.5) * horasextra
        salario = (horas - horasextra) * tarifa
        total = valorextra + salario
        print("El valor del salario con horas extra es: ", total)
    elif horas <= 40:
        print("El valor del salario es:", salario)
except:
    print("Error, por favor introduzca un número")