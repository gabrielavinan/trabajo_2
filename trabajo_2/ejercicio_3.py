#___author___ "Gabriela Viñan"
#___ email___ gabriela.vinan@unl.edu.ec
#Escribe un programa que solicite una puntuación entre 0.0 y 1.0. Si lapuntuación está fuera de ese
# rango, muestra un mensaje de error. Si la puntuación está entre 0.0 y 1.0,

try:
    puntuacion= float(input("Inprese la puntuación:"))
    if puntuacion >=0 and puntuacion <= 1.0:
        if puntuacion >= 0.9:
            print("Sobresaliente")
        elif puntuacion >=0.8:

            print("Notable")
        elif puntuacion >= 0.7:
            print("Bien")
        elif puntuacion >= 0.6:
            print("Suficiente")
        elif puntuacion < 0.6:
            print("Inuficiente")
    else:
        print("Putuacion incorrecta")
except:
    print("Puntuacion incorrecta")